import pandas as pd
import time
import datetime
GPU = "gpu_uuid"
USERNAME = "User Name"
MEM = " used_gpu_memory [MiB]"
KEEPING_HEADLINE = [" pid","User Name"," process_name", GPU, MEM,"current_hour","current_time","current_day","current_month","current_year"]


def merge_proc_and_gru(gpu_path, proc_path):
    proc = pd.read_csv(proc_path)
    gpu = pd.read_csv(gpu_path)

    id_list = gpu[" pid"].tolist()
    proc_df = proc[proc['PID'].isin(id_list)]
    proc_df = proc_df[["PID", "User Name"]]
    proc_df = pd.merge(gpu, proc_df, left_on=' pid',right_on='PID',how='left')
    proc_df["current_time"] = time.time()
    now = datetime.datetime.now()
    proc_df["current_hour"] = now.hour
    proc_df["current_day"] = now.day
    proc_df["current_month"] = now.month
    proc_df["current_year"] = now.year
    proc_df = proc_df[KEEPING_HEADLINE]
    return proc_df


def get_num_hour_of_a_user(username, df, in_month=None, in_year=None):
    df = get_all_information_of_a_user(username, df, in_month, in_year)
    return str(df.shape[1]/12)

def get_all_information_of_a_user(username, df, in_month=None, in_year=None):
    df = df[df[USERNAME] == username]
    month, year = get_month_and_year(in_month, in_year)
    df = df[df["current_month"] == month]
    df = df[df["current_year"] == year]
    return df


def get_month_and_year(in_month, in_year):
    now = datetime.datetime.now()
    month = None
    year = None

    if in_month is None:
        month = str(now.month)
    else:
        month = str(in_month)

    if in_year is None:
        year = str(now.year)
    else:
        year = str(in_year)

    return month, year


def all_user_name(df):
    return list(df[USERNAME].unique())



