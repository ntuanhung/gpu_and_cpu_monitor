import threading, sched, time
import subprocess
from analyze_data import merge_proc_and_gru, KEEPING_HEADLINE
from config import *
import pandas as pd
import datetime
import os


def get_gpu_and_tasklist():
    now = datetime.datetime.now()
    subprocess.run(["nvidia-smi.exe", "--query-compute-apps=gpu_uuid,timestamp,pid,process_name,used_memory", "--format=csv"], stdout=open(GPU_FILE, "wt"))
    subprocess.run(["tasklist", "/v", "/fo", "csv"], stdout=open(TASKLIST_FILE, "wt"))

    current_info_df = merge_proc_and_gru(GPU_FILE, TASKLIST_FILE)

    current_file_name = FULL_TASK%(now.month, now.year)
    if os.path.isfile(current_file_name):
        all_data = pd.read_csv(current_file_name)
        all_data = all_data.append(current_info_df)
    else:
        all_data = current_info_df
    all_data = all_data[KEEPING_HEADLINE]
    all_data.to_csv(current_file_name)


if __name__ == "__main__":
    print("start collect data")
    while True:
        get_gpu_and_tasklist()
        time.sleep(DELAYED_TIME)
