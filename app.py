from flask import Flask, request
from config import *
from analyze_data import *
app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Call url /user_info with get request: \n' \
           '+ key: username, in_month, in_year to get using hours \n' \
           '\n' \
           'Call url /all_username to get all username \n'


@app.route('/user_info')
def get_user_info():
    username = request.args.get('username')
    in_month = request.args.get('in_month')
    in_year = request.args.get('in_year')

    month, year = get_month_and_year(in_month, in_year)
    df = pd.read_csv(FULL_TASK%(month, year))

    return '''<h1>num of using hours: {}</h1>'''.format(get_num_hour_of_a_user(username, df, in_month, in_year))


@app.route('/all_username')
def get_all_username_info():
    month, year = get_month_and_year(None, None)
    df = pd.read_csv(FULL_TASK%(month, year))

    return str(all_user_name(df))


if __name__ == '__main__':
    app.run()
